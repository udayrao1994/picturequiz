let express = require("express");
const dotenv = require('dotenv').config();
const cors = require("cors");
let app = express();
app.use(cors());
app.use(express.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});
//const port = 2410;
var port = process.env.PORT || 2410;
app.listen(port, () => console.log(`Node app listening on port ${port}!`));




// let mysql = require("mysql");
// let connData = {
//   host: "localhost",
//   user: "root",
//   password: "",
//   database: "testdb3",
// };
const {Client}=require("pg");
const client=new Client({
user:process.env.USERNAME,
password:process.env.DBPASSWORD,
database:process.env.DBNAME,
port:process.env.PORTNO,
host:process.env.DBHOST,
ssl:{ rejectUnauthorized: false },
})
console.log("username",process.env.DB_USER);
client.connect(function (res, error) {console.log(`Connected!!!`);});

app.get("/quizData/:name", function (req, res, next)

{console.log("Inside /quizdata get api");
let name=req.params.name;
const query = ` SELECT * FROM quizdata where playername = '${name}'`;
console.log("query",query)
client.query(query, function (err, result) 
{if (err) {
    console.log(err);
      res.status(400).send(err);
   }
   else
      res.send(result.rows);

    //client.end();
   });
});

app.post("/quizData", function (req, res, next) {
  console.log("Inside post of quiz");

  var values = Object.values(req.body);
  console.log(values);
  const query = `INSERT INTO quizdata (errorcount,playername,retrycount,minutes,seconds)VALUES ($1,$2,$3,$4,$5)`;
  client.query(query, values, function (err, result) {
      if (err) {
          console.log(err);
          res.status(400).send(err);
      }
      //console.log(result);
      res.send(result);
  });
});