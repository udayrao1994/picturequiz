import axios from "axios";

const  basedURL="https://desolate-sierra-40990.herokuapp.com";

function get(url)
{
    console.log("axios.get(basedURL+url)",axios.get(basedURL+url));
    return axios.get(basedURL+url);

}

function post(url,obj)
{

    return axios.post(basedURL+url,obj)
}

function deleteApi(url){
console.log("basedURL+url",basedURL+url)
    return axios.delete(basedURL+url)
}

export default{

    get,
    post,
    deleteApi,
};