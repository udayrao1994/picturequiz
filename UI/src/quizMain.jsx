import React, { Component } from "react";
import moment from "moment";

import Quiz from "./quiz";

import http from "./httpService.js";
import QuizResult from "./quizResult";

class QuizMain extends Component {
  state = {
    detail: [
      {
        imgurl:
          "https://www.welovequizzes.com/wp-content/uploads/2021/02/pexels-pas%CC%A6ca-daniel-3954963.jpg?ezimgfmt=ng:webp/ngcb1.jpg",
        text: "Which file format is ideal for Web photos?",
        options: ["Tif", "Gif", "png", "jpeg"],
        answer: 3,
        answered: -1,
      },
      {
        imgurl:
          "https://www.welovequizzes.com/wp-content/uploads/2021/02/pexels-pas%CC%A6ca-daniel-3954963.jpg?ezimgfmt=ng:webp/ngcb1.jpg",
        text: "What is the name of this flower? ",
        options: ["Lilies", "Poppies", "Daisies"],
        answer: 1,
        answered: -1,
      },
      {
        imgurl:
          "https://cdn.shopclues.com/images1/thumbnails/113393/320/320/152076452-113393756-1615182920.jpg",
        text: "Which type of Bulb ?",
        options: ["Incandescent bulbs", "Halogen bulbs", "LED bulbs"],
        answer: 2,
        answered: -1,
      },
      {
        imgurl: "https://m.media-amazon.com/images/I/61yhoYzp6rL._SL1500_.jpg",
        text: " Which mouse click is used to select item?  ",
        options: [
          "Single click",
          "Double click",
          "Right click",
          "Drag and drop",
        ],
        answer: 1,
        answered: -1,
      },

      {
        imgurl:
          "https://media.proprofs.com/images/QM/user_images/1826446/1610548951.jpg",
        text: " The acronym PC stands for:",
        options: ["Private Computer", "Personal Computer", "Personal Compact"],
        answer: 1,
        answered: -1,
      },

      {
        imgurl:
          "https://images.firstpost.com/wp-content/uploads/2021/01/pedro-lastra-br-Xdb9KE0Q-unsplash-1.jpg ",
        text: " What is the name of the Animal?  ",
        options: ["Starfish", "Dolphin", "Snake", "Seahorse"],
        answer: 0,
        answered: -1,
      },
      {
        imgurl: "https://static.dw.com/image/15943674_6.jpg",
        text: " What is the name of the animal ?",
        options: ["Dolphin", "Star Fish", "Snake", "Shark"],
        answer: 0,
        answered: -1,
      },

      {
        imgurl:
          "https://media.proprofs.com/images/QM/user_images/2503852/New%20Project%20(65)(63).jpg ",
        text: "_____is used in an operating system to separate mechanism from policy.",
        options: [
          "Single level implementation",
          "None",
          "Two level implementation",
          "Multi level implementation",
        ],
        answer: 2,
        answered: -1,
      },
      {
        imgurl:
          "https://i.natgeofe.com/n/101c7d2c-d45a-4579-a63c-450c4bac73e4/snakes_06.jpg",
        text: "What is the name of the Animal?",
        options: ["Snake", "Elephant", "Dog", "Fish"],
        answer: 0,
        answered: -1,
      },
      {
        imgurl:
          "https://media.proprofs.com/images/QM/user_images/2503852/New%20Project%20(65)(63).jpg ",
        text: " The operating system creates _____ from the physical computer.",
        options: [
          "Virtual space",
          "Virtual device",
          "None",
          "Virtual computers",
        ],
        answer: 3,
        answered: -1,
      },
      {
        imgurl:
          "https://media.proprofs.com/images/QM/user_images/2503852/New%20Project%20(65)(63).jpg ",
        text: " Which is built directly on the hardware?",
        options: [
          "Computer Environment",
          "Application Software",
          "Operating System",
          "Database System",
        ],
        answer: 2,
        answered: -1,
      },
      {
        imgurl:
          "https://media.proprofs.com/images/QM/user_images/2503852/New%20Project%20(28)(83).jpg ",
        text: " Media Access Control (MAC) and Logical Link Control (LLC) are two sublayers of _________________.",
        options: [
          "Session layer",
          "Network layer",
          "Transport layer",
          "Data Link layer",
        ],
        answer: 2,
        answered: -1,
      },
      {
        imgurl:
          "https://media.proprofs.com/images/QM/user_images/2503852/New%20Project%20(28)(83).jpg",
        text: " Which of the following is true for coaxial cables?",
        options: [
          "It uses light rays instead of electricity to transmit data",

          "Coaxial cables provide effective protection against EMI during data transmissions.",
          "It can be differentiated into two categories, single-mode and multimode",

          "It uses copper wires, which are good conductors of electricity.",
        ],
        answer: 3,
        answered: -1,
      },
    ],

    questionsSet: [],
    view: 2,
    currentIndex: 0,
    reload: true,
    errorcount: 0,
    playername: "",
    retrycount: 1,
    resultdetails: [],
    errortext: "",
    starttime: "",
    finishtime: "",
  };

  handleChange = (e) => {
    let { currentTarget: input } = e;

    let s1 = { ...this.state };
    s1.playername = input.value;
    this.setState(s1);
  };

  start = () => {
    let s1 = { ...this.state };
    s1.starttime = new Date();

    if (s1.playername == "") {
      s1.errortext = "Please enter name to proceed.";
    } else {
      s1.view = 0;
      s1.errortext = "";
    }

    this.setState(s1);
  };

  next = (index) => {
    let s1 = { ...this.state };

    s1.reload = false;

    let currentquestion = s1.questionsSet[index - 1];

    if (currentquestion.answer == currentquestion.answered) {
      if (index > -1) s1.currentIndex = index;
      s1.errortext = "";
    } else {
      s1.errorcount++;
      s1.errortext = "Please choose correct answer.Your answer is wrong";
    }

    this.setState(s1);
  };

  isRepeat = (s1, question) => {
    let index = s1.questionsSet.findIndex(
      (ele) => ele.text == question.text && ele.imgurl == question.imgurl
    );

    if (index != -1) {
      return true;
    }
  };
  resetquestion = (s1) => {
    s1.detail.map((ele) => (ele.answered = -1));
    if (s1.reload == true) {
      s1.questionsSet = [];
      let questioncount = 0;
      while (questioncount != 5) {
        let x = Math.floor(Math.random() * 12 + 0);
        let question = s1.detail[x];
        if (!this.isRepeat(s1, question)) {
          s1.questionsSet.push(question);
          questioncount++;
        }
      }
    }

    this.setState(s1);
  };

  componentWillMount() {
    let s1 = { ...this.state };
    s1.errortext = "";
    this.resetquestion(s1);
  }

  addanswrered = (optindex) => {
    let s1 = { ...this.state };

    let obj = s1.questionsSet[s1.currentIndex];

    obj.answered = optindex;

    this.setState(s1);
  };

  Finish = () => {
    let s1 = { ...this.state };
    s1.finishtime = new Date();

    const startDate = moment(s1.starttime);
    const timeEnd = moment(s1.finishtime);
    const diff = timeEnd.diff(startDate);
    const diffDuration = moment.duration(diff);

    let data = {
      errorcount: s1.errorcount,
      playername: s1.playername,
      retrycount: s1.retrycount,
      minutes: diffDuration.minutes(),
      seconds: diffDuration.seconds(),
    };

    this.postData("/quizData", data);

    
  };

  async postData(url, obj) {
    let response = await http.post(url, obj);
    let s1 = { ...this.state };
    s1.view = 1;
    this.setState(s1);


  }
  onRetry = () => {
    let s1 = { ...this.state };
    s1.view = 0;
    s1.reload = true;

    s1.currentIndex = 0;
    s1.retrycount++;

    this.resetquestion(s1);
  };
  previous = (index) => {
    let s1 = { ...this.state };
    if (index <= s1.detail.length - 1) s1.currentIndex = index;

    this.setState(s1);
  };

  render() {
    let { currentIndex, questionsSet, errortext, view, playername } =
      this.state;

    let currentdetail = questionsSet[currentIndex];
    return view === 0 ? (
      <div className="container">
        <h4 className="text-center">Welcome to Quiz App</h4>

        <Quiz
          details={currentdetail}
          index={currentIndex}
          next={this.next}
          addans={this.addanswrered}
          onPrevious={this.previous}
          onFinish={this.Finish}
          errormsg={errortext}
        />
      </div>
    ) : view == 1 ? (
      <QuizResult onRetry={this.onRetry} playername={playername} />
    ) : (
      <div className="container">
        <div className="form-group m-20">
          <label>
            <h5>Please Enter Your Name To Begin Quiz</h5>
          </label>

          <input
            type="text "
            className="form-control"
            id="name"
            name="name"
            placeholder="Enter Name"
            onChange={this.handleChange}
          />
          {errortext ? <span className="text-danger">{errortext}</span> : ""}
          <br></br>
          <div>
            {" "}
            <button className="btn btm-sm bg-primary" onClick={this.start}>
              Start Quiz
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default QuizMain;
