import React, { Component } from "react";
import http from "./httpService.js";
class QuizResult extends Component {
  state = {
    resultdetails: [],
  };
  componentDidMount() {
    this.getResult();
  }

  
  async getResult() {
    let response = await http.get(`/quizData/${this.props.playername}`);

    let { data } = response;

    this.setState({ resultdetails: data });
  }

  render() {
    let { resultdetails } = this.state;

    return (
      <React.Fragment>
        <h4 className="text-center ">Quiz App Result</h4>
        <div className="container text-center">
          <div className="row bg-dark text-light">
            <div className="col-3 border">No</div>
            <div className="col-3 border">Player Name</div>
            <div className="col-3 border">Eror Count</div>

            <div className="col-3 border">Time Taken this time</div>
          </div>
          {resultdetails.map((ele, index) => (
            <div className="row">
              <div className="col-3 border">{index + 1}</div>
              <div className="col-3 border"> {ele.playername}</div>
              <div className="col-3 border"> {ele.errorcount}</div>

              <div className="col-3 border">
                {" "}
                {ele.minutes}
                {" Minute(s):"}
                {ele.seconds}
                {" Second(s)"}
              </div>
            </div>
          ))}
          <br />
          <button
            className="btn btn-primary"
            onClick={() => this.props.onRetry()}
          >
            {" "}
            Retry
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default QuizResult;
